export default {
    'GET /api/test': (req: any, res: any) => {
        res.json({ data: 'GET' })
    },
    'POST /api/test': (req: any, res: any) => {
        res.json({ data: 'POST' })
    },
    "PUT /api/test": (req: any, res: any) => {
        res.json({ data: 'PUT' })
    },
    "DELETE /api/test": (req: any, res: any) => {
        res.json({ data: 'DELETE' })
    },
    "/api/test": (req: any, res: any) => {
        res.json({ data: 'all' })
    },
}