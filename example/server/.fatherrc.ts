export default [{
  entry:'index.ts',
  file:'lib/index.js',
  target: 'node',
  cjs: { type: 'babel', lazy: true },
  disableTypeCheck: true,
  extraBabelPlugins: [
    [
      'babel-plugin-import',
      { libraryName: 'antd', libraryDirectory: 'es', style: true },
      'antd',
    ],
  ],
}];
